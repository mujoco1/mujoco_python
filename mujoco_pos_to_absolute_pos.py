# -*- coding: utf-8 -*-
"""
Created on Sat Feb 20 11:23:22 2021


Use /home/erdi/python3.7/bin/ this python interpreter

@author: Erdi
"""

import xml.etree.ElementTree as ET
import numpy as np
from scipy.spatial.transform import Rotation as R



sr_hand_path = "/home/erdi/Desktop/Storage/test/mujoco_shadow_hand/assets/hand/robot_touch_sensors_92.xml"
tree = ET.parse(sr_hand_path)
root = tree.getroot()
# k = np.array([list(elem.attrib.values()) for elem in root.iter("body")])

# print(k)


#Getting all the lower joint values
# for movie in root.findall("./body"):
#     print(movie.attrib)
    

find_name = "robot0:T_ffproximal_front_left_bottom"        

tmp = []


def Transformation_matrix(euler,pos):
    transformation_matrix = np.eye(4)
    r = R.from_euler('xyz',euler)
    transformation_matrix[0:3,0:3] = r.as_matrix()
    transformation_matrix[0:3,3] = pos
    print(transformation_matrix)
    return transformation_matrix

    

flag_name_found = False
transformation_matrix_with_respect_to_world_frame = np.eye(4)
for elem in root.iter():


    if "body" == elem.tag:
        tmp.append(elem.tag)
        if "pos" in elem.attrib:
            position_str = elem.attrib["pos"].split(" ")
            position = [ np.float64(i) for i in position_str]
        else:
            position = np.zeros([1,3])

        if "euler" in elem.attrib:
             euler_rotation_str = elem.attrib["euler"].split(" ")
             euler_rotation = [ np.float64(i) for i in euler_rotation_str]
        else:
            euler_rotation = np.zeros([1,3])

        transformation_matrix_with_respect_to_world_frame = transformation_matrix_with_respect_to_world_frame @ Transformation_matrix(euler_rotation,position) 

    if "name" in elem.attrib:
        if elem.attrib['name'] == find_name:
            flag_name_found = True
            print("here found")
            break

    # print(elem.tag)

if not flag_name_found:
    print("The searched element is not found")
else: 
    print("The searched element is found")

print(transformation_matrix_with_respect_to_world_frame)

r = R.from_matrix(transformation_matrix_with_respect_to_world_frame[0:3,0:3])
print(r.as_euler('xyz'))
print(transformation_matrix_with_respect_to_world_frame[0:3,3])




# tmp_matrix = np.array([[-1.00000000e+00, -2.65358979e-06,  0.00000000e+00],[-9.74717957e-12,  3.67320510e-06, -1.00000000e+00],[ 2.65358979e-06, -1.00000000e+00, -3.67320510e-06]])

# print(tmp_matrix)

# r = R.from_matrix(tmp_matrix)
# print(r.as_euler('xyz'))